"""Prueba_4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Prueba_4.views.home import v_index
from Prueba_4.views.galeria import v_galeria
from Prueba_4.views.contacto import contacto_index, formulario_contacto
from Prueba_4.views import login
from Prueba_4.views import logout
from Prueba_4.views.mantenedorcontacto import load_contacto
from django.contrib.auth.models import Permission, ContentType
from Prueba_4.models import Contacto
from Prueba_4.views import errorpage
from Prueba_4.api.v1 import apiv1
from Prueba_4.views.tienda import v_tienda

admin.site.register(Permission)
admin.site.register(ContentType)
admin.site.register(Contacto)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', v_index),
    path('', v_index),
    path('galeria/', v_galeria),
    path('contacto/', contacto_index),
    path('contacto/formulario', formulario_contacto),
    path('mantenedor-contacto/', load_contacto),
    path('login', login.index),  
    path('logout/', logout.logout_user),
    path('error-401/', errorpage.error_401_page),
    path('error-403/', errorpage.error_403_page),
    path('api/v1/contacto', apiv1.contacto),
    path('tienda/', v_tienda),
]
