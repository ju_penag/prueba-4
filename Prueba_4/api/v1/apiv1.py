from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from rest_framework import status
import json
from Prueba_4.models import Contacto

@api_view(['GET', 'POST'])
def contacto(request):
    if request.method == 'GET':
        js = {
                'Dev1':{
                    'rut': '26.972.352-1',
                    'nombre': 'Juan Peña'
                },
                'Dev2':{
                    'rut': '26.892.051-k',
                    'nombre': 'Marcos Olmedo'
                },
                'Dev3':{
                    'rut': '26.972.352-1',
                    'nombre': 'Tyhara Mujica'
                }                
            }
        status_code = status.HTTP_200_OK
        print('response_json', js)
        return HttpResponse(json.dumps(js, ensure_ascii=False), content_type="application/json", status=status_code)
